# TICOM

### Objetivo

El sitio Web de TICOM Studios, pagina web con los webcomics y tutoriales

### Diseño de pagina principal

Poner el Grid de fondo en la parte de abajo y flotanto los recuadros que enlazan a los comics,
usar sprites para poner una parte del ultimo comic / blog asi como contactos y soporte

Crear un RSS maestro de todo el contenido

Volver a la idea de que se basa en la interfaz de KIN, solo que con
los colores de la "marca", hacerlo en glitch.com, ver si se puede
emular el "spot" como el menu de compartir en redes sociales/ver en
medium/tapas/etc.

Ver como hacer esa fila de elementos de diferente tamaño, chance con
flexbox apilados?, o con un grid que contenga objetos de diferente
tamaño?, empezar con una cuadricula

Ver como hacerlo tipo App, usando JS para cargar los elementos sin
tener que escribir un monton de html, la idea es tener un JSON con los
enlaces y demas

### Formato

El del comic de Matrix, por velocidad

### Secciones por comic

- Pagina principal con la ultima tira y enlaces relevantes
- Descripcion general del webcomic, acerca de
- Elenco principal
- Archivo con las tiras, con el orden cronologico de eventos y el orden de publicacion
- Un blog sobre el comic
- feed CSS
- Enlaces a Patreon, Kofi, Medium, Twitter, Mastodon y GitLab hacer los logos, ponerlos flotando en una seccion verde en homenage del "Spot" del KIN, el de los cuadros
- El premier del universo, mas que nada para compartirlo y desarollarlo fuera del documento ORG (?)

## Made by [Glitch](https://glitch.com/)

\ ゜ o ゜)ノ
